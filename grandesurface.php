<?php
/*
class Dentifrices {
	private $name;
	private $reference;
	...
	//code spécifique aux Dentifrices
}
class Shampooings{
	private $name;
	private $reference;
	...
	//code spécifique aux Shampooings
}
[........]
class Farines {
	private $name;
	private $reference;
	...
	//code spécifique aux Farines
}
 
class Catalogues {
    public function __construct() {
        $dentifrice = new Dentifrices;
        $shampooing = new Shampooings;
        ...
        $farine = new Farines;
    }
}
 
$catalogue = new Catalogues;
*/
// Classe abstract de base
abstract class Produit {
	private $name;
	private $reference;
}
// Class spe par produi
class Shampooings extends Produit {
	private $var_specifique;
	/* code spe */
}
class Farines extends Produit {
	private $var_specifique;
	/* code spe */
}
class Dentifrices extends Produit {
	private $var_specifique;
	/* code spe */
}
// Classe usine 
class FactoProd {
	public static function createProduit ($produit){
		switch ($produit){
			case 'Dentifrices':
				$produit = new Dentifrices;
				break;
			case 'Shampooings':
				$produit = new Shampooings;
				break;
			case 'Farines':
				$produit = new Farines;
				break;
			default:
				throw new Exception ('Produit Inconnu');
		}
		return $produit;
	}
}
// Classe catalogue
class Catalogues {
    public function __construct() {
		$dentifrice = FactoProd::createProduit("Dentifrices");
        $shampooing = FactoProd::createProduit("Shampooings");;
        $farine = FactoProd::createProduit("Farines");;
    }
}
 
$catalogue = new Catalogues;
?>