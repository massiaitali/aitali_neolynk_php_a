<?php 
$TabValAlea = array();
for ($i=0; $i < 20; $i++) { 
	array_push($TabValAlea, rand(1, 100));
}
class automate {
	private $tabValeur;
	private $valJoueur;

	public function __construct($tabV, $valJ) {
		$this->tabValeur = $tabV;
		$this->valJoueur = $valJ;
	}
	// Simple recherche sur les deux tableau avec un complexité n*n
	public function solveSimple() {
		for ($i=0; $i <sizeof($this->tabValeur) ; $i++) { 
			for ($j=0; $j <sizeof($this->tabValeur) ; $j++) { 
				$addition = $this->tabValeur[$i] + $this->tabValeur[$j];
				if($addition == $this->valJoueur) {
					return true;
				}
			}
		}
		return false;
	}
	// Recherche un peu plus inteligente
	public function solveComplexe() {
		$tabValeurTrie = $this->tabValeur;
		sort($tabValeurTrie);
		$isFinish = true;
		$i = 0;
		$j = sizeof($tabValeurTrie)-1;
		while($isFinish) {
			$addition = $tabValeurTrie[$i] + $tabValeurTrie[$j];
			if($addition == $this->valJoueur) {
				return true;
			}
			else {
				if($addition > $this->valJoueur){
					$j = $j - 1;
				}
				else {
					$i = $i + 1;
				}
			}
			if ($i == $j) {
				$isFinish = false;
			}
		}
		return false;
	}
}
print "Saisir un chiffre entre 1 et 100 (compris) : ";
$val = (int)trim(fgets(STDIN));
if ($val <= 0 || $val > 100) {
	echo "Le chiffre d'entrée ne respecte pas la regle : 100>=Chiffre>0 et de type entier\n";
}
else {
	$automateTest = new automate($TabValAlea, $val);
	if($automateTest->solveComplexe()) {
		echo "Gagné\n";
	}			
	else {
		echo "Perdu\n";
	}
}
?>